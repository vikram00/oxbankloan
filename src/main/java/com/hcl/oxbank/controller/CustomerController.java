package com.hcl.oxbank.controller;

import com.hcl.oxbank.Utils.ApplicationConstants;
import com.hcl.oxbank.dto.CustomerDto;
import com.hcl.oxbank.exception.DateException;
import com.hcl.oxbank.service.CustomerService;
import com.hcl.oxbank.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
public class CustomerController {
    @Autowired

    public CustomerService customerService;
    public LoanService loanService;

    @PostMapping(ApplicationConstants.customersave)
    public String customerSave(@Valid @RequestBody CustomerDto customerDto) throws DateException {
        String dobString = customerDto.getDob();
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(ApplicationConstants.pattern);
            formatter.setLenient(false);
            Date dob = formatter.parse(dobString);
            Date currentDate = new Date();
            if (dob.after(currentDate)) {
                throw new DateException(ApplicationConstants.futures);
            }
        } catch (ParseException e) {
            throw new DateException(ApplicationConstants.pareser);
        }
        customerService.datasave(customerDto);
        return ApplicationConstants.res;
    }
}